/**
 * En este script se declaran las imporaciones del proyecto
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
//var methodOverride = require('method-override');
var mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({
    extended : true
}));

//Configuración de cabeceras y cors
//Los cors dan permisos para las solicitudes al backend, es decir, da mayor seguridad.

app.use( (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    res.header('Access-Control-Allow-Headers', 
    'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
//rutas
app.use(require('./src/routers/routes'));
//Se expone la página principal
app.use(express.static('./src/public'));

//Se expone la aplicación de express al resto del proyecto
module.exports = app;