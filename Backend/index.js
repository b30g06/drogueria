var app = require('./app');
const mongoose = require('./src/connDB/conn');
var port = process.env.PORT || 4000;

//El servidor espera solicitudes
app.listen(port, () => {
    console.log(`Servidor siendo ejecutado en el puerto: ${port}`);
});
