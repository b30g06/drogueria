//Importamos el paquete mongoose
const mongoose = require('mongoose');

/**
 * Credenciales MongoDB
 * usr : admondrogueriaG06
 * psw : dG2iELydp9Ynt8B
 * mongodb+srv://admondrogueriaG06:<password>@cluster0.zzgc0.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
 */

const db = "drogueriadb";
const usr = "admondrogueriaG06";
const psw = "dG2iELydp9Ynt8B";

uri = `mongodb+srv://${usr}:${psw}@cluster0.zzgc0.mongodb.net/${db}?retryWrites=true&w=majority`
mongoose.connect(
    //"mongodb://localhost:27017/drogueriadb",
    uri,
    {
        useNewUrlParser : true,
        //useCreateIndex : true,
        useUnifiedTopology : true,
        //useFindAndModify : false,   
    },
    (err, res) => {
        //Si la conexión se realiza con éxito o no.
        if (err) {
            throw err;
        } else {
            console.log('La conexión a la base de datos fue exitosa.');
        }
    }
);

//Exponemos el módulo con la conexión mediante mongoose
module.exports = mongoose;