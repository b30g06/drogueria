const Articulo = require("../models/articulo");

function test( req, res) {
    res.status(200).send({
        message : "Prueba de funcionamiento de ruta"
    });
}

function saveArticulo( req, res) {
    //console.log(req.body);
    let myArticulo = new Articulo(req.body);
    myArticulo.save( (err, result) => {
        res.status(200).send( {
            message : result
        });
    });
}

function buscarArticulo( req, res) {
    //Obtenemos el id de los parámetros de la petición
    let idArticulo = req.params.id;
    Articulo.findById( idArticulo).exec( (err, result) => {
        if (err) {
            res.status(500).send({message : 'Error al momento de ejecutar la soliccitud'});
        } else {
            if (!result) {
                res.status(404).send({message : `No se encontró el artículo con id ${idArticulo}.`});
            } else {
                res.status(200).send({result});
            }
        }
    });
}

function listarArticulos( req, res) {
    //Obtenemos el id de los parámetros de la petición
    let idArticulo = req.params.idb;

    if (!idArticulo) {
        var result = Articulo.find({});
    } else {
        var result = Articulo.find({_id: idArticulo});
    }

    result.exec(function(err, result) {
        if (err) {
            res.status(500).send({message : 'Error al momento de ejecutar la solicitud.'});
        } else {
            if (!result) {
                res.status(404).send({message : `No se encontró el artículo con id ${idArticulo}.`});
            } else {
                res.status(200).send({result});
            }
        }
    });
}

function updateArticulo( req, res) {
    console.log("Actualizar artículo.");
    let idArticulo = req.params.id;

    if (!idArticulo) {
        res.status(404).send({
            message : 'id del artículo no especificado.'
        })
    } else {
        Articulo.findOneAndUpdate({_id: idArticulo}, req.body, {new: true}, (err, articulo) => {
            if (err) {
                res.status(500).send({
                    message : 'Error al hacer la actualización.',
                    error : err
                });
                res.json(articulo);
            } else {
                if (!articulo){
                    res.status(404).send({
                        message : `No se encontró el artículo con id ${idArticulo}`
                    })
                } else {
                    res.status(200).send({articulo});
                }
                
            }
        }); 
    }
}

function deleteArticulo(req, res) {
    console.log("Eliminar artículo.");
    let idArticulo = req.params.id;
    if (!idArticulo) {
        res.status(404).send({
            message : 'id del artículo no especificado'
        });
    } else {
        Articulo.findByIdAndRemove(idArticulo, (err, articulo) => {
            if (err) {
                res.status(500).send({
                    message : "F: Ocurruó un error al ejecutar la eliminación."
                });
            } else {
                if (!articulo) {
                    res.status(404).send({
                        message : `No se encontró un artículo con el id ${idArticulo}.`
                    });
                } else {
                    res.status(200).send({
                        message : "Artículo eliminado.",
                        articulo : articulo
                    });
                }
            }
        });
    }
}

module.exports = {
    test,
    saveArticulo,
    buscarArticulo,
    listarArticulos,
    updateArticulo,
    deleteArticulo
}