var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ArticuloSchema = Schema({
    id : Number,
    nombre : String,
    img : String,
    categoria : String,
    precio : Number
});

const Articulo = mongoose.model('articulo', ArticuloSchema);
//Exponemos el módulo para ser usado en el proyecto
module.exports = Articulo;