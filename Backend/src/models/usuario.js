var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var usuarioSchema = Schema({
    id : Number,
    nombre : String,
    carrito : {
        articulos : [
            {
                id : Number,
                nombre : String,
                precio : Number
            }
         ]
    }
});

const Usuario = mongoose.model('usuario', usuarioSchema);
//Exponemos el módulo para ser usado en el proyecto
module.exports = Usuario;