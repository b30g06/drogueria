const { Router } = require('express');
var articuloController = require('../controllers/articuloController');
const router = Router();

router.get('/test', articuloController.test);
router.post('/saveArticulo', articuloController.saveArticulo);
router.post('/buscarArticulos/:id', articuloController.buscarArticulo);
router.post('/buscarArticulos/:id?', articuloController.listarArticulos);
router.put('/articulo/:id', articuloController.updateArticulo);
router.delete('/articulo/:id', articuloController.deleteArticulo);

module.exports = router;
