import { Component, OnInit } from '@angular/core';
import { ProductDataHolderService } from './product/product-data-holder.service';
import { ProductModel } from './product/product-model';
import { ConnHttpService } from './services/conn-http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Frontend';

  /* products:ProductModel[] = [
    {
      id: 0,
      nombre: "aspirina",
      img: "https://images.rappi.com/products/2090614625-1582998627551.jpg?d=200x200&?d=1920xundefined&e=webp",
      categoria: "analgísico",
      precio: 800
    },
    {
      id: 1,
      nombre: "naspirina",
      img: "https://images.rappi.com/products/2090614625-1582998627551.jpg?d=200x200&?d=1920xundefined&e=webp",
      categoria: "anabólico",
      precio: 1800
    },
    {
      id: 2,
      nombre: "raspirina",
      img: "https://images.rappi.com/products/2090614625-1582998627551.jpg?d=200x200&?d=1920xundefined&e=webp",
      categoria: "anestésico",
      precio: 81800
    },
  ] */

  //products!:ProductModel[];
  //Los datos de productos se toman del servicio ProductDataHolderService
  products$ = this.productsData.products$;
  editProduct$ = this.productsData.editProduct$;

/*   editProduct: ProductModel = {
    nombre: "",
    img: "",
    categoria: "",
    precio: 0
  } */

  get editProduct(): ProductModel {
    return this.productsData.getEditProduct();
  }

  set editProduct(value: ProductModel) {
    this.productsData.updateEditProduct(value)
  }

  constructor(
    private conn: ConnHttpService,
    private productsData: ProductDataHolderService
    ) { }

  ngOnInit() {
    this.conn.getProducts().subscribe(
      (data) => {
        //console.log(data.result);
        this.productsData.updateProducts(data.result);
      }
    );
  }

  onClickEditProduct(){
    this.conn.editProduct(this.productsData.getEditProduct()).subscribe(
      (resp) => {
        console.log(resp);
      }
    );
  }

  onClickDeleteProduct() {
    this.conn.deleteProduct(this.productsData.getEditProduct()).subscribe(
      (resp) => {
        //console.log(resp);
        this.productsData.deleteProduct();
      }
    )
  }
}
