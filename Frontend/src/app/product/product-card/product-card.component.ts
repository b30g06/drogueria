import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AdminStatusService } from 'src/app/services/admin-status.service';
import { ProductDataHolderService } from '../product-data-holder.service';
import { ProductModel } from '../product-model';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit, OnDestroy {
  //@Input para que datos pueda tomarse como una propiedad y enlazar con datos en la plantilla
  // productData toma los valores cuando se instancia este componente con ngFor
  @Input() productData!:ProductModel;
  @Input() index!:number;
  isAdmin!:boolean;
  private adminSubspcription!:Subscription;

  constructor(
    private adminStatusService: AdminStatusService,
    private productsData: ProductDataHolderService
    ) { }

  ngOnInit(): void {
    this.adminSubspcription = this.adminStatusService.admin$.subscribe(
      (admin) => {
        this.isAdmin = admin;
      }
    )
  }

  ngOnDestroy(): void {
    this.adminSubspcription.unsubscribe();
  }

  onClickedEdit() {
    //console.log(this.productData);
    this.productsData.updateEditProduct(this.productData);
    this.productsData.indexEditProduct = this.index;
  }
}
