import { TestBed } from '@angular/core/testing';

import { ProductDataHolderService } from './product-data-holder.service';

describe('ProductDataHolderService', () => {
  let service: ProductDataHolderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductDataHolderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
