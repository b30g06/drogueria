import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ProductModel } from './product-model';

@Injectable({
  providedIn: 'root'
})
export class ProductDataHolderService {
/* Este servicio va a contener los datos de los productos y
   permite que otros componentes se subscriban a esta información. */
  private products = new BehaviorSubject<ProductModel[]>([]);
  products$ = this.products.asObservable();

  private editProduct = new BehaviorSubject<ProductModel>({
    nombre: "",
    img: "",
    categoria: "",
    precio: 0
  });
  editProduct$ = this.editProduct.asObservable();
  indexEditProduct!: number;

  constructor() { }

  updateProducts(newProducts: ProductModel[]) {
    this.products.next(newProducts);
  }

  getProducts() {
    return this.products.value;
  }

  addProduct(newProduct: ProductModel) {
    let arr = this.products.value;
    arr.push(newProduct);
    console.log(arr);
    this.products.next(arr);
  }

  deleteProduct() {
    let arr = this.getProducts();
    arr.splice(this.indexEditProduct, 1);
    this.products.next(arr);
  }

  updateEditProduct(newEditProduct: ProductModel) {
    this.editProduct.next(newEditProduct);
  }

  getEditProduct(){
    return this.editProduct.value;
  }
}
