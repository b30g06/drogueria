export interface ProductModel {
    _id?: string,
    id?: number;
    nombre: string;
    img: string;
    categoria: string;
    precio: number;
}
