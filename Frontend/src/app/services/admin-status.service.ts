import { Injectable, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminStatusService {
  private admin = new BehaviorSubject<boolean>(true);
  admin$ = this.admin.asObservable();

  constructor() { }

  adminModeOn():void{
    this.admin.next(true);
  }
  
  adminModeOff():void{
    this.admin.next(false);
  }
}
