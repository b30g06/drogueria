import { TestBed } from '@angular/core/testing';

import { ConnHttpService } from './conn-http.service';

describe('ConnHttpService', () => {
  let service: ConnHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
