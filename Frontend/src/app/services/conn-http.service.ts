import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ProductModel } from '../product/product-model';

@Injectable({
  providedIn: 'root'
})
export class ConnHttpService {

  private server = "https://tudroga.herokuapp.com";
  //private port = process.env.PORT || 4000;
  private uri = `${this.server}`;

  constructor(private http:HttpClient) { }

  getProducts(){
    let _uri = `${this.uri}/buscarArticulos/`;
    //console.log("Link: "+_uri);
    return this.http.post<any>(_uri, null);
  }

  createProduct(product: ProductModel) {
    let _uri = `${this.uri}/saveArticulo/`;
    return this.http.post<any>(_uri, product);
  }

  editProduct(product: ProductModel) {
    let _uri = `${this.uri}/articulo/${product._id}`;
    return this.http.put<any>(_uri, product);
  }

  deleteProduct(product: ProductModel) {
    let _uri = `${this.uri}/articulo/${product._id}`;
    return this.http.delete<any>(_uri);
  }
}
