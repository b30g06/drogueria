import { Component, Input, OnInit } from '@angular/core';
import { ProductDataHolderService } from 'src/app/product/product-data-holder.service';
import { ProductModel } from 'src/app/product/product-model';
import { AdminStatusService } from 'src/app/services/admin-status.service';
import { ConnHttpService } from 'src/app/services/conn-http.service';

@Component({
  selector: 'app-user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.css']
})
export class UserDropdownComponent implements OnInit {

  isAdmin:boolean = false;

  newProduct: ProductModel = {
    nombre: "",
    img: "",
    categoria: "",
    precio: 0
  }

  constructor(
    private adminStatusService: AdminStatusService,
    private conn: ConnHttpService,
    private productsData: ProductDataHolderService
    ) { }

  ngOnInit(): void {
    this.adminStatusService.admin$.subscribe(
      (admin) => {
        this.isAdmin = admin;
      }
    )
  }

  onClickAdminMode(): void {
    this.adminStatusService.adminModeOn();
  }

  onClickPublicMode(): void {
    this.adminStatusService.adminModeOff();
  }

  onClickAddProduct() :void {
    this.conn.createProduct(this.newProduct).subscribe(
      (resp) => {
        console.log(resp);
        this.productsData.addProduct(resp.message);
      }
    );
  }

}
