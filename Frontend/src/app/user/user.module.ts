import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDropdownComponent } from './user-dropdown/user-dropdown.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    UserDropdownComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    UserDropdownComponent
  ]
})
export class UserModule { }
